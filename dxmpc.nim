import wNim
import os
import browsers
import strutils
import strformat
import json
proc toStrArray*(js:seq[JsonNode]):seq[string]=
    for entry in js:
        result.add(entry.getStr)
        
proc toStrArray*(js:JsonNode):seq[string]=
    for entry in js.getElems():
        result.add(entry.getStr)
        
let manifest = parseFile(getCurrentDir()&"/manifest.json")
echo manifest
var found = false
var curl_exe = findExe("curl.exe")
echo "Curl not installed. (Not W10?) Using local version."

if hostCPU == "amd64": curl_exe = getCurrentDir()&"/curl64/curl.exe"
    
if hostCPU == "i386": curl_exe = getCurrentDir()&"/curl32/curl.exe"
let 
    possible_paths = manifest["deusex_paths"].toStrArray()
    app = App()
    frame = Frame(title="DXMP Community")
    panel = Panel(frame)

frame.center() # Center frame window in screen
frame.show() # A frame is hidden on creation by default.
StaticText(panel, pos = (0, 0), label="Architecture: "&hostCPU&" | Curl path: "&curl_exe&" | DXMPC Compiled on "&CompileTime&" "&CompileDate)

let path = TextCtrl(panel, size = (500, 20), pos = (0, 20))
path.writeText("Finding Deus Ex installation...")

for p in possible_paths:
    if dirExists(p):
        path.clear()
        path.writeText(p&"/")
        found = true

if not found: path.writeText("Deus Ex not found, manually enter path...")
let kenties = CheckBox(panel, label = "Using Kenties Launcher", pos = (0, 40))


let opts = ListBox(panel, pos = (0, 65), size = (300, 200), style = wLbNeededScroll)
opts.append(manifest["actions_list"].toStrArray())

var sel_title = StaticText(panel, pos = (400, 75), size = (200, 30), label = "Nothing selected....")
var sel_desc = StaticText(panel, pos = (310, 100), size = (300, 150), label = "")
var selected_ls = ""
let act = Button(panel, pos = (400, 250), label = "Run")


proc fileReplace(fp, sfrom, sto: string):int =
    if fileExists(path.getValue()&fp):
        var 
            dict = readFile(path.getValue()&fp) 
            outp = dict.count(sfrom)
          
        while(sfrom in dict):
            dict = dict.replace(sfrom, sto)
            
        if outp > 0:
            writeFile(path.getValue()&fp, dict)
        return outp
    else:
        MessageDialog(panel, message = "File "&path.getValue()&fp&" not found.").display()
        return-1
    
proc openURL(url:string) =
    var u = url
    if u.startsWith("$"):
        let key = u.split(".")[0].replace("$", "")
        let value = u.split(".")[1]
        u = manifest[key][value].getStr("")
    echo "Opening "&u
    if u != "":
        openDefaultBrowser(u)
    
act.wEvent_Button do ():
    echo "Button pressed on "&selected_ls
    
    if manifest["action_data"].hasKey(selected_ls):
        if manifest["action_data"][selected_ls].hasKey("action"):
            let action = manifest["action_data"][selected_ls]["action"]
            let params = action["options"]
            case action["type"].getStr:
                of "webBrowser":
                    echo "Opening web"
                    openURL params["url"].getStr
                    
                of "fileEdit":
                    echo "Editing file"
                    var done = 0
                    for f in params["files"]:

                        for tor in params["subs"]:
                            done += fileReplace(f.getStr, tor[0].getStr, tor[1].getStr)

                        if kenties.getValue() == true and f.getStr == "System/DeusEx.ini":
                            # TODO  also edit the file in Documents, need to get the path for it...
                            #done += fileReplace(f.getStr, tor[0].getStr, tor[1].getStr)
                            echo "null"

                    if done == 0:
                        MessageDialog(panel, message = "No changes made.").display()
                    else:
                        MessageDialog(panel, message = "Changes complete.").display()
                else:
                    echo "Unknown action."
        else:
            echo "Fallback!"
        
    else:
        echo "Do nothing!"

proc handleListClick(s:string) =
    if manifest["action_data"].hasKey(s):
        sel_title.destroy()
        sel_desc.destroy()
        sel_title = StaticText(panel, pos = (400, 75), size = (200, 30), label = manifest["action_data"][s]["title"].getStr())
        sel_desc = StaticText(panel, pos = (310, 100), size = (300, 150), label = manifest["action_data"][s]["desc"].getStr())
    else:
        sel_title.destroy()
        sel_desc.destroy()
        sel_title = StaticText(panel, pos = (400, 75), size = (200, 30), label = "Nothing selected....")
        sel_desc = StaticText(panel, pos = (310, 100), size = (300, 150), label = "")
        
opts.wEvent_ListBox do ():
    selected_ls = opts.getText(opts.getSelection())
    handleListClick(selected_ls)
    
    
    
StaticText(panel, pos = (0, 270), size = (600, 100), label=fmt"""Welcome to the Deus Ex Multiplayer Community Executable.
Clicking any of the buttons initiates the task. Make sure to tick the Using Kenties button if you're using Kenties launcher without --localdata, otherwise some edits won't work due to that launcher moving some files.
This executable is open-source, written in Nim {NimVersion}, and compiled for both 32bit and 64bit.
It uses CURL for downloading external files. On Windows 10, the internal version is used. For previous versions of Windows, a local version is provided (7.77.0_2).""")
#[]
**Cozmo's All-In-One**: This is a self-contained update of Deus Ex ready for multiplayer, containing various patches. Masterserver update and Render Device update is included in this.

**Update Masterserver**: This updates a base install of Deus Ex to use the community masterserver, courtesy of 333networks.

**Render Device drivers**: Installs Donahl's updated renderers.

**Launch.exe**: Installs Han's Launch.exe launcher.


""")
]#


app.mainLoop()
